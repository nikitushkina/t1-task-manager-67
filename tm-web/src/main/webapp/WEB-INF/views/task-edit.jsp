<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>TASK EDIT</h1>

<form:form action="/task/edit/${task.id}" method="POST" modelAttribute="task">
<form:input type="hidden" path="id" />

<p>
    <div>NAME:</div>
    <div><form:input type="text" path="name"/></div>
</p>

<p>
    <div>DESCRIPTION:</div>
    <div><form:input type="text" path="description"/></div>
</p>

<p>
    <div>PROJECT:</div>
    <form:select path="project">
        <form:option value="${null}" label="--- // ---"/>
        <form:options items="${projects}" itemLabel="name" itemValue="id"/>
    </form:select>
</p>

<p>
    <div>STATUS:</div>
    <form:select path="status">
        <form:option value="${null}" label="--- // ---"/>
        <form:options items="${statuses}" itemLabel="displayName"/>
    </form:select>
</p>

<p>
    <div>DATE CREATED:</div>
        <div><form:input type="date" path="created"/>
    </div>
</p>

    <button type="submit" style="background-color: white; color: indianred">SAVE TASK</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>