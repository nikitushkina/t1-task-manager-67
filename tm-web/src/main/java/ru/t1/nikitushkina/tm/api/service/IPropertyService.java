package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getDBUser();

    @NotNull
    String getDBPassword();

    @NotNull
    String getDBUrl();

    @NotNull
    String getDBDriver();

    @NotNull
    String getDBDialect();

    @NotNull
    String getDBShowSql();

    @NotNull
    String getFormatSQL();

    @NotNull
    String getDBSchema();

    @NotNull
    String getDBHbm2ddlAuto();

    @NotNull
    String getDBFormatSql();

    @NotNull
    String getDBSecondLvlCache();

    @NotNull
    String getDBFactoryClass();

    @NotNull
    String getDBUseQueryCache();

    @NotNull
    String getDBUseMinPuts();

    @NotNull
    String getDBRegionPrefix();

    @NotNull
    String getDBConfigFilePath();

}
