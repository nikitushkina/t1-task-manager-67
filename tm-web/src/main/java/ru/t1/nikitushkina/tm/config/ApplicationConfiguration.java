package ru.t1.nikitushkina.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.nikitushkina.tm")
public class ApplicationConfiguration {
}
