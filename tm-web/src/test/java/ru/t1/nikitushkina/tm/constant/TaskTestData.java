package java.ru.t1.nikitushkina.tm.constant;

import org.jetbrains.annotations.NotNull;

public class TaskTestData {

    @NotNull
    public final static String USER_TASK1_NAME = "Test Task 01";

    @NotNull
    public final static String USER_TASK1_DESCRIPTION = "Test Task Description 01";

    @NotNull
    public final static String USER_TASK2_NAME = "Test Task 02";

    @NotNull
    public final static String USER_TASK2_DESCRIPTION = "Test Task Description 02";

    @NotNull
    public final static String USER_TASK3_NAME = "Test Task 03";

    @NotNull
    public final static String USER_TASK3_DESCRIPTION = "Test Task Description 03";

}
