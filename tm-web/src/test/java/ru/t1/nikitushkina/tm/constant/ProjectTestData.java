package java.ru.t1.nikitushkina.tm.constant;

import org.jetbrains.annotations.NotNull;

public class ProjectTestData {

    @NotNull
    public final static String USER_PROJECT1_NAME = "Test Project 01";

    @NotNull
    public final static String USER_PROJECT1_DESCRIPTION = "Test Project Description 01";

    @NotNull
    public final static String USER_PROJECT2_NAME = "Test Project 02";

    @NotNull
    public final static String USER_PROJECT2_DESCRIPTION = "Test Project Description 02";

    @NotNull
    public final static String USER_PROJECT3_NAME = "Test Project 03";

    @NotNull
    public final static String USER_PROJECT3_DESCRIPTION = "Test Project Description 03";

}
