package ru.t1.nikitushkina.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;
import ru.t1.nikitushkina.tm.enumerated.Role;

public interface IUserDTOService extends IDTOService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable final String login, @Nullable final String password) throws Exception;

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final String email) throws Exception;

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final Role role) throws Exception;

    @NotNull
    UserDTO create(@Nullable final String login,
                   @Nullable final String password,
                   @Nullable final String email,
                   @Nullable final String lastName,
                   @Nullable final String firstName,
                   @Nullable final String middleName) throws Exception;

    @Nullable
    UserDTO findByEmail(@Nullable final String email) throws Exception;

    @Nullable
    UserDTO findByLogin(@Nullable final String login) throws Exception;

    Boolean isEmailExists(@Nullable final String email) throws Exception;

    Boolean isLoginExists(@Nullable final String login) throws Exception;

    void lockUserByLogin(@Nullable final String login) throws Exception;

    void removeByLogin(@Nullable final String login) throws Exception;

    void setPassword(@Nullable final String id, @Nullable final String password) throws Exception;

    void unlockUserByLogin(@Nullable final String login) throws Exception;

    void updateUser(@Nullable final String id,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName) throws Exception;

}
