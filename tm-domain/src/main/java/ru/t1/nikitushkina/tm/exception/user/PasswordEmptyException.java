package ru.t1.nikitushkina.tm.exception.user;

public final class PasswordEmptyException extends AbstractUserException {

    public PasswordEmptyException() {
        super("Error! Password is empty.");
    }

}
